#pragma once
#include <string>

class Message
{
private:
	// fields
	std::string first_username;
	std::string second_username;
	std::string content;

public:
	// c'tor
	Message(std::string first, std::string second, std::string chat);
	// getters
	std::string getFirstUsername();
	std::string getSecondUsername();
	std::string getContent();
	std::string getFileName();
};
