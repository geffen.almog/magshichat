#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <thread>
#include <string>
#include <queue>
#include <mutex>
#include <vector>
#include <fstream>
#include "Message.h"

class Server
{
public:
	Server();
	~Server();
	void serve(int port);

private:
	std::mutex mu; // mutex
	std::vector<std::string> users; // all of the users
	std::condition_variable cond; // condition var
	std::queue<Message> messages; // queue of messages

	void accept();
	void clientHandler(SOCKET clientSocket);
	void messageHandler();

	SOCKET _serverSocket;
};
