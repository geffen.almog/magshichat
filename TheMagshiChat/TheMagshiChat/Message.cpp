#include "Message.h"

/*
* c'tor
*/
Message::Message(std::string first, std::string second, std::string chat) : first_username(first), second_username(second), content(chat)//c'tor
{
}

/*
* getting the first username
* input: none
* output: string of first username
*/
std::string Message::getFirstUsername()
{
	return first_username;
}

/*
* getting the second username
* input: none
* output: string of second username
*/
std::string Message::getSecondUsername()
{
	return second_username;
}

/*
* getting the content
* input: none
* output: string of the message content
*/
std::string Message::getContent()
{
	return content;
}

/*
* getting the accuret file name, by order of the alphabet
* input: none
* output: none
*/
std::string Message::getFileName()
{
	if (first_username.compare(second_username) < 0) // comparing the names for the file name to be correct
	{
		return first_username + "&" + second_username + ".txt";  // if the first username is before the second username in the alaphabet
	}
	else
	{
		return second_username + "&" + first_username + ".txt"; // if the second username is before the first username in the alaphabet
	}
}
