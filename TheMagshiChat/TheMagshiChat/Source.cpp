#pragma comment (lib, "ws2_32.lib")

#include "Server.h"
#include <thread>
#include <iostream>
#include <exception>
#include "WSAInitializer.h"

int main()
{
	try
	{
		WSAInitializer wsaInit; // intalizing the was
		Server theServer; // creating the server
		std::fstream portAndIPfile("config.txt"); // creating the file that includes the port
		std::string line; // creating string for line

		while (std::getline(portAndIPfile, line)) // getting the port
		{
			if (line.find("port") != std::string::npos) // finding the port
			{
				line = line.substr(line.find('=') + 1); // parsing and substringing
				break;
			}
		}
		theServer.serve(atoi(line.c_str())); // starting to work
	}
	catch (std::exception& e) // catching incase there's an error
	{
		std::cout << "Error occured: " << e.what() << std::endl; // alertting the error
	}
	system("PAUSE"); // pausing the system
	return 0;
}