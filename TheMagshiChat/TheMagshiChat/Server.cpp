#include "Server.h"
#include <exception>
#include <iostream>
#include "Helper.h"
#include "Message.h"

// max length for message
#define MAX 105

// code length
#define CODE_LEN 3

// codes beggining
#define LOGIN_CODE 200
#define CLIENT_CODE 204

// lengths
#define NAME_INDEX 5
#define BYTESLENGTH 2

// bytes defines
#define TWOBYTES 10
#define THREEBYETS 100
#define FOURBYTES 1000
#define FIVEBYTES 10000

#define SERVER_UPDATE_MSG "1010000000"

/*
 * function constructs a server object and creates the socket
 * input: none
 * output: none
 */
Server::Server()
{
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP); // creating the socket
	if (_serverSocket == INVALID_SOCKET) // thorowing exception if something went wrong
		throw std::exception(__FUNCTION__ " - socket");
}

/*
 * function destructs a Server obj, closes the socket
 * input: none
 * output: none
 */
Server::~Server()
{
	try // trying to colsing the server
	{
		closesocket(_serverSocket); // closing socket
	}
	catch (...) {}
}

/*
 * function serves a client - does the basic things(bind, listen, etc.)
 * input: param port - the port to talk with the client
 * output: none
 */
void Server::serve(int port)
{
	struct sockaddr_in sa = { 0 };
	std::cout << "Starting..." << std::endl;

	// making the socket fine
	sa.sin_port = htons(port); // address family: AF_INET 
	sa.sin_family = AF_INET; // port in network byte order 
	sa.sin_addr.s_addr = INADDR_ANY;  // internet address 

	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR) // binding
		throw std::exception(__FUNCTION__ " - bind"); // throwing exception if some went wrong

	std::cout << "Binding..." << std::endl;

	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR) // starting to listen
		throw std::exception(__FUNCTION__ " - listen"); // throwing exception if some went wrong

	std::cout << "Listenning..." << std::endl;
	std::cout << "Waiting for clients to join..." << std::endl;

	while (true) // waiting for client connection, accepting when happens
	{
		accept(); // calling to accept
	}
}

/*
 * function accepts the new client, then calls the clientHandler
 * input: none
 * output: none
 */
void Server::accept()
{
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL); // accepting the client

	std::cout << "accepting client..." << std::endl;

	if (client_socket == INVALID_SOCKET) // throwing if given invalid socket - the client connection isn't working
		throw std::exception(__FUNCTION__);

	std::cout << "Client has been accepted!" << std::endl;

	std::thread newClient{ &Server::clientHandler, this, client_socket }; // creating the thread for this specific client-server talk
	newClient.detach(); // detaching it from the main thread
}

/*
* this function does everything. this is my master creation
* input: param socket - the socket to talk with the client
* output: none
*/
void Server::clientHandler(SOCKET clientSocket)
{
	std::string theUsername = ""; // creating string for name
	std::string serverUpdateMsg = ""; // creating string for sever message 
	std::thread msgThread{ &Server::messageHandler, this }; // creating the message thread
	msgThread.detach(); // detaching it from the main thread

	while (true) // infinite loop
	{
		try
		{
			char m[MAX]; // string to recieve messages
			recv(clientSocket, m, MAX - 1, 0); // recieving the message from the client

			std::string msg = m; // converting the chars to string
			int code = atoi(msg.substr(0, CODE_LEN).c_str()); // slicing the code

			if (code == LOGIN_CODE) // if type of code is - login code, code beggins with '200'
			{
				std::string lengthOfAllActiveUsersNames; // string for the length of all active users names
				int lengthOfName = atoi(msg.substr(CODE_LEN, BYTESLENGTH).c_str()); // slicing the name lenght and converting it to int
				std::string stringlengthOfName = std::to_string(lengthOfName);
				theUsername = msg.substr(NAME_INDEX, lengthOfName); // slicing the current name
				users.push_back(theUsername); // adding the current name to the users

				std::cout << "Added new client " << std::to_string(clientSocket) << ", " << theUsername << " to client list" << std::endl;

				if (lengthOfName < TWOBYTES) // making the username length for the msg
				{
					// adding the matching padding to length that smaller than two bytes
					lengthOfAllActiveUsersNames = "0000" + stringlengthOfName;
				}
				else if (lengthOfName < THREEBYETS)
				{
					// adding the matching padding to length that smaller than three bytes
					lengthOfAllActiveUsersNames = "000" + stringlengthOfName;
				}
				else if (lengthOfName < FOURBYTES)
				{
					// adding the matching padding to length that smaller than four bytes
					lengthOfAllActiveUsersNames = "00" + stringlengthOfName;
				}
				else if (lengthOfName < FIVEBYTES)
				{
					// adding the matching padding to length that smaller than five bytes
					lengthOfAllActiveUsersNames = "0" + stringlengthOfName;
				}
				else
				{
					// if there's no length, adding the matching padding
					lengthOfAllActiveUsersNames = "10000";
				}
				serverUpdateMsg = SERVER_UPDATE_MSG + lengthOfAllActiveUsersNames + theUsername;
				send(clientSocket, serverUpdateMsg.c_str(), serverUpdateMsg.length(), 0); // sending server update message to client
			}

			else if (code == CLIENT_CODE) // if type of code is - login code, code beggins with '204'
			{
				std::string secondUsername = ""; // string for second username
				std::string newMessage = ""; // string for new message
				std::string content = ""; // string for cotent of file
				std::string line; // line from file
				std::string stringOfAllActiveUsers = ""; // string for all active users names
				std::string theFileName = ""; // the file name
				std::fstream chatFile; // file of the chat

				auto lengthOfSecondUserName = atoi(msg.substr(CODE_LEN, BYTESLENGTH).c_str()); // slicing the name length and converting it to int
				auto lengthOfNewMessage = 0; // creating length of new message

				if (msg.length() > NAME_INDEX + lengthOfSecondUserName) // checking if its a regular update or including message sending
				{
					lengthOfNewMessage = atoi(msg.substr(NAME_INDEX + lengthOfSecondUserName, NAME_INDEX).c_str()); // slicing the message length and converting it to int
				}
				secondUsername = msg.substr(NAME_INDEX, lengthOfSecondUserName); // slicing the second username from the message
				if (lengthOfSecondUserName && lengthOfNewMessage) // sending message
				{
					newMessage = msg.substr(NAME_INDEX * BYTESLENGTH + lengthOfSecondUserName, lengthOfNewMessage); // slicing the message content from the msg
					std::string messageContent = "&MAGSH_MESSAGE&&Author&" + theUsername + "&DATA&" + newMessage;
					Message temp(theUsername, secondUsername, messageContent); // creating message object with first username, second username and message content
					messages.push(temp); // pushing to the queue and notifying the waiting thread
					cond.notify_all(); // notifying
				}

				if (theUsername.compare(secondUsername) < 0) // comparing the names for the file name to be correct
				{
					theFileName = (theUsername + "&" + secondUsername + ".txt"); // if the first username is before the second username in the alaphabet
				}
				else
				{
					theFileName = (secondUsername + "&" + theUsername + ".txt"); // if the second username is before the first username in the alaphabet
				}

				chatFile.open(theFileName); // openning file
				if (chatFile.is_open())
				{
					while (std::getline(chatFile, line)) // getting content of file 
					{
						content += line; // adding each line
					}
				}
				chatFile.close(); // closing the file

				for (auto& user : users) // getting all usersnames
				{
					stringOfAllActiveUsers += user + "&"; // adding each username to string of all users names
				}

				stringOfAllActiveUsers = stringOfAllActiveUsers.substr(0, stringOfAllActiveUsers.length() - 1); // removing the last &
				Helper::send_update_message_to_client(clientSocket, content, secondUsername, stringOfAllActiveUsers); // sending the message to client
			}
		}

		catch (const std::exception& e) // incase some user dissconnected
		{
			for (auto i = users.begin(); i != users.end(); ++i) // deleting the logged out user from the vector
			{
				if (!i->compare(theUsername)) // comparing
				{
					users.erase(i); // earesing
					return;
				}
			}
			closesocket(clientSocket); //closing the client's socket
		}
	}
}

/*
* function writes to the file the message needed, also works with unique lock
* input: none
* output: none
*/
void Server::messageHandler()
{
	while (true) // infinite loop 
	{
		std::unique_lock<std::mutex> messageLocker(mu); // mutex for the message
		cond.wait(messageLocker); // waiting for cond notifying
		while (!messages.empty()) // while there are still messages
		{
			std::fstream theChatFile; // creating the chat file
			std::string message = messages.front().getContent(); // getting the whole message
			std::string line = ""; // creating string for line
			std::string content = ""; // creating string for message content

			theChatFile.open(messages.front().getFileName(), std::ios::app); // opening the file with append setting

			if (theChatFile.is_open()) // if file can be properly opened
			{
				theChatFile << message; // writing to the file
			}
			messages.pop(); // popping
			messageLocker.unlock(); // unlocking the mutex
			theChatFile.close(); // closing the chat file
		}
	}
}